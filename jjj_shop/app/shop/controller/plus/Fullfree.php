<?php

namespace app\shop\controller\plus;

use app\common\enum\settings\SettingEnum;
use app\shop\controller\Controller;
use app\shop\model\settings\Setting as SettingModel;

/**
 * 满额包邮控制器
 */
class Fullfree extends Controller
{
    /**
     *商品推荐
     */
    public function index()
    {
        if($this->request->isGet()){
            $vars['values'] = SettingModel::getItem(SettingEnum::FULL_FREE);
            return $this->renderSuccess('', compact('vars'));
        }
        $model = new SettingModel;
        if ($model->edit(SettingEnum::FULL_FREE, $this->postData())) {
            return $this->renderSuccess('操作成功');
        }
        return $this->renderError($model->getError() ?:'操作失败');
    }

}